## Docker_Tutorial_for_Beginners
* https://www.youtube.com/watch?v=3c-iBn73dDE
* [screenshots](screenshots)

```
0:00:00 - Intro and Course Overview
0:01:58 - What is Docker?
0:10:56 - What is a Container?
0:19:40 - Docker vs Virtual Machine
0:23:53 - Docker Installation
0:42:02 - Main Docker Commands
0:57:15 - Debugging a Container
1:06:39 - Demo Project Overview - Docker in Practice
1:10:08 - Developing with Containers
1:29:49 - Docker Compose - Running multiple services
1:42:02 - Dockerfile - Building our own Docker Image
2:04:36 - Private Docker Repository - Pushing our built Docker Image into a private Registry on AWS
2:19:06 - Deploy our containerized app
2:27:26 - Docker Volumes - Persist data in Docker
2:33:03 - Volumes Demo - Configure persistence for our demo project
2:45:13 - Wrap Up
```

```
1. What is Docker?
* What is a container and what problems does it solve?
* Container repository - where do containers live?

2. What is a Container technically
* What is a container technically? (layers of images)
* Demo part (docker hub and run a docker container locally)

3. Docker vs Virtual Machine

4. Docker Installation
* Before Installing Docker - prerequisites
* Install docker on Mac, Windows, Linux
* Note: Docker Toolbox has been deprecated. 
  Please use Docker Desktop instead. 
  See for Mac (https://docs.docker.com/docker-for-mac/) and for 
  Windows (https://docs.docker.com/docker-for-wi...).

5. Main Docker Commands
* docker pull, docker run, docker ps, docker stop, docker start, port mapping

6. Debugging a Container
* docker logs, docker exec -it

7. Demo Project Overview - Docker in Practice (Nodejs App with MongoDB and MongoExpress UI)

8. Developing with Containers 
* JavaScript App (HTML, JavaScript Frontend, Node.js Backend)
* MongoDB and Mongo Express Set-Up with Docker
* Docker Network concept and demo

9. Docker Compose - Running multiple services
* What is Docker Compose?
* How to use it - Create the Docker Compose File
* Docker Networking in Docker Compose

10. Dockerfile - Building our own Docker Image
* What is a Dockerfile?
* Create the Dockerfile
* Build an image with Dockerfile

11. Private Docker Repository - Pushing our built Docker Image into a private Registry on AWS
* Private Repository on AWS ECR
* docker login
* docker tag
* Push Docker Image to the Private Repo

12. Deploy our containerized application

13. Docker Volumes - Persist data in Docker
* When do we need Docker Volumes?
* What is Docker Volumes?
* Docker Volumes Types

14. Volumes Demo - Configure persistence for our demo project
```

```
git clone https://gitlab.com/nanuchi/techworld-js-docker-demo-app
```


---



```
docker run postgresql:9.6
docker ps
docker run postgresql:10.10
```

```
docker run redis
docker ps
docker run -d redis
docker stop
docker start
docker ps -a
docker pull redis:4.0
docker run -p6000:6379 redis
docker run -p6000:6379 -d redis
docker run -p6001:6379 redis:4.0
docker images
```

* `docker run`  # create a new container (from image)
* `docker start` # start existing container

```
docker logs
docker logs dreamy_bell
docker run -d -p6001:6379 --name redis-older redis:4.0
docker exec -it redis-older /bin/bash
```

```
docker pull mongo
docker pull mongo-express
docker images
docker network ls
docker network create mongo-network
docker network ls

docker run -d \
  -p 27017:27017 \
  -e MONGO_INITDB_ROOT_USERNAME=admin \
  -e MONGO_INITDB_ROOT_PASSWORD=password \
  --name mongodb --net mongo-network \
  mongo

docker run -d \
 -p 8081:8081
 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
 -e ME_CONFIG_MONGODB_ADMINPASSWORD=password \
 --net mongo-network
 --name mongo-express
 -e ME_CONFIG_MONGODB_SERVER=mongodb \
 mongo-express
 
docker logs mongodb -f 
```

```
docker-compose -f mongo.yaml up -d
docker ps
docker network ls
docker-compose -f mongo.yaml down
docker ps
docker network ls
```

```
docker build -t my-app:1.0 .
docker images
docker run my-app:1.0
docker rm  ccc
docker rmi iii
docker run my-app:1.0
dpocker logs lll
docker exec -it eee /bin/sh
```

